﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ServicioVeterinariaRest.Models;

namespace ServicioVeterinariaRest.Controllers
{
    public class PersonalController : ApiController
    {
        private ModelVeterinariaContainer db = new ModelVeterinariaContainer();

        // GET api/Personal
        public IQueryable<personal> Getpersonals()
        {
            return db.personals;
        }

        // GET api/Personal/5
        [ResponseType(typeof(personal))]
        public IHttpActionResult Getpersonal(int id)
        {
            personal personal = db.personals.Find(id);
            if (personal == null)
            {
                return NotFound();
            }

            return Ok(personal);
        }

        // PUT api/Personal/5
        public IHttpActionResult Putpersonal(int id, personal personal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != personal.id)
            {
                return BadRequest();
            }

            db.Entry(personal).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!personalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Personal
        [ResponseType(typeof(personal))]
        public IHttpActionResult Postpersonal(personal personal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.personals.Add(personal);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = personal.id }, personal);
        }

        // DELETE api/Personal/5
        [ResponseType(typeof(personal))]
        public IHttpActionResult Deletepersonal(int id)
        {
            personal personal = db.personals.Find(id);
            if (personal == null)
            {
                return NotFound();
            }

            db.personals.Remove(personal);
            db.SaveChanges();

            return Ok(personal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool personalExists(int id)
        {
            return db.personals.Count(e => e.id == id) > 0;
        }
    }
}