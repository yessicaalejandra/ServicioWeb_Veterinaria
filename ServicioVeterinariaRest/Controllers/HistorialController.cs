﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ServicioVeterinariaRest.Models;

namespace ServicioVeterinariaRest.Controllers
{
    public class HistorialController : ApiController
    {
        private ModelVeterinariaContainer db = new ModelVeterinariaContainer();

        // GET api/Historial
        public IQueryable<historial> Gethistorials()
        {
            return db.historials;
        }

        // GET api/Historial/5
        [ResponseType(typeof(historial))]
        public IHttpActionResult Gethistorial(int id)
        {
            historial historial = db.historials.Find(id);
            if (historial == null)
            {
                return NotFound();
            }

            return Ok(historial);
        }

        // PUT api/Historial/5
        public IHttpActionResult Puthistorial(int id, historial historial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != historial.id)
            {
                return BadRequest();
            }

            db.Entry(historial).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!historialExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Historial
        [ResponseType(typeof(historial))]
        public IHttpActionResult Posthistorial(historial historial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.historials.Add(historial);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = historial.id }, historial);
        }

        // DELETE api/Historial/5
        [ResponseType(typeof(historial))]
        public IHttpActionResult Deletehistorial(int id)
        {
            historial historial = db.historials.Find(id);
            if (historial == null)
            {
                return NotFound();
            }

            db.historials.Remove(historial);
            db.SaveChanges();

            return Ok(historial);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool historialExists(int id)
        {
            return db.historials.Count(e => e.id == id) > 0;
        }
    }
}