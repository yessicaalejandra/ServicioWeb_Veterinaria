﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ServicioVeterinariaRest.Models;

namespace ServicioVeterinariaRest.Controllers
{
    public class MascotaController : ApiController
    {
        private ModelVeterinariaContainer db = new ModelVeterinariaContainer();

        // GET api/Mascota
        public IQueryable<mascota> Getmascotas()
        {
            return db.mascotas;
        }

        // GET api/Mascota/5
        [ResponseType(typeof(mascota))]
        public IHttpActionResult Getmascota(int id)
        {
            mascota mascota = db.mascotas.Find(id);
            if (mascota == null)
            {
                return NotFound();
            }

            return Ok(mascota);
        }

        // PUT api/Mascota/5
        public IHttpActionResult Putmascota(int id, mascota mascota)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mascota.id)
            {
                return BadRequest();
            }

            db.Entry(mascota).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!mascotaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Mascota
        [ResponseType(typeof(mascota))]
        public IHttpActionResult Postmascota(mascota mascota)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.mascotas.Add(mascota);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mascota.id }, mascota);
        }

        // DELETE api/Mascota/5
        [ResponseType(typeof(mascota))]
        public IHttpActionResult Deletemascota(int id)
        {
            mascota mascota = db.mascotas.Find(id);
            if (mascota == null)
            {
                return NotFound();
            }

            db.mascotas.Remove(mascota);
            db.SaveChanges();

            return Ok(mascota);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool mascotaExists(int id)
        {
            return db.mascotas.Count(e => e.id == id) > 0;
        }
    }
}