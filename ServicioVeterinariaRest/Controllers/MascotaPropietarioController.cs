﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ServicioVeterinariaRest.Models;

namespace ServicioVeterinariaRest.Controllers
{
    public class MascotaPropietarioController : ApiController
    {
        private ModelVeterinariaContainer db = new ModelVeterinariaContainer();

        // GET api/MascotaPropietario
        public IQueryable<mascotapropietario> Getmascotapropietarios()
        {
            return db.mascotapropietarios;
        }

        // GET api/MascotaPropietario/5
        [ResponseType(typeof(mascotapropietario))]
        public IHttpActionResult Getmascotapropietario(int id)
        {
            mascotapropietario mascotapropietario = db.mascotapropietarios.Find(id);
            if (mascotapropietario == null)
            {
                return NotFound();
            }

            return Ok(mascotapropietario);
        }

        // PUT api/MascotaPropietario/5
        public IHttpActionResult Putmascotapropietario(int id, mascotapropietario mascotapropietario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mascotapropietario.fkmascota)
            {
                return BadRequest();
            }

            db.Entry(mascotapropietario).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!mascotapropietarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/MascotaPropietario
        [ResponseType(typeof(mascotapropietario))]
        public IHttpActionResult Postmascotapropietario(mascotapropietario mascotapropietario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.mascotapropietarios.Add(mascotapropietario);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (mascotapropietarioExists(mascotapropietario.fkmascota))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = mascotapropietario.fkmascota }, mascotapropietario);
        }

        // DELETE api/MascotaPropietario/5
        [ResponseType(typeof(mascotapropietario))]
        public IHttpActionResult Deletemascotapropietario(int id)
        {
            mascotapropietario mascotapropietario = db.mascotapropietarios.Find(id);
            if (mascotapropietario == null)
            {
                return NotFound();
            }

            db.mascotapropietarios.Remove(mascotapropietario);
            db.SaveChanges();

            return Ok(mascotapropietario);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool mascotapropietarioExists(int id)
        {
            return db.mascotapropietarios.Count(e => e.fkmascota == id) > 0;
        }
    }
}