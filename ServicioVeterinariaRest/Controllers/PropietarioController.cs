﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ServicioVeterinariaRest.Models;

namespace ServicioVeterinariaRest.Controllers
{
    public class PropietarioController : ApiController
    {
        private ModelVeterinariaContainer db = new ModelVeterinariaContainer();

        // GET api/Propietario
        public IQueryable<propietario> Getpropietarios()
        {
            return db.propietarios;
        }

        // GET api/Propietario/5
        [ResponseType(typeof(propietario))]
        public IHttpActionResult Getpropietario(int id)
        {
            propietario propietario = db.propietarios.Find(id);
            if (propietario == null)
            {
                return NotFound();
            }

            return Ok(propietario);
        }

        // PUT api/Propietario/5
        public IHttpActionResult Putpropietario(int id, propietario propietario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != propietario.id)
            {
                return BadRequest();
            }

            db.Entry(propietario).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!propietarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Propietario
        [ResponseType(typeof(propietario))]
        public IHttpActionResult Postpropietario(propietario propietario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.propietarios.Add(propietario);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = propietario.id }, propietario);
        }

        // DELETE api/Propietario/5
        [ResponseType(typeof(propietario))]
        public IHttpActionResult Deletepropietario(int id)
        {
            propietario propietario = db.propietarios.Find(id);
            if (propietario == null)
            {
                return NotFound();
            }

            db.propietarios.Remove(propietario);
            db.SaveChanges();

            return Ok(propietario);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool propietarioExists(int id)
        {
            return db.propietarios.Count(e => e.id == id) > 0;
        }
    }
}