
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/28/2015 16:48:33
-- Generated from EDMX file: C:\Users\USUARIO\Desktop\ServicioVeterinariaRest\ServicioVeterinariaRest\ServicioVeterinariaRest\Models\ModelVeterinaria.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [veterinaria];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_fkmascona]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[historials] DROP CONSTRAINT [FK_fkmascona];
GO
IF OBJECT_ID(N'[dbo].[FK_fkpersonal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[historials] DROP CONSTRAINT [FK_fkpersonal];
GO
IF OBJECT_ID(N'[dbo].[FK_fkmascota]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[mascotapropietarios] DROP CONSTRAINT [FK_fkmascota];
GO
IF OBJECT_ID(N'[dbo].[FK_fkpropietario]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[mascotapropietarios] DROP CONSTRAINT [FK_fkpropietario];
GO
IF OBJECT_ID(N'[dbo].[FK_fkpagina1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[rolpaginas] DROP CONSTRAINT [FK_fkpagina1];
GO
IF OBJECT_ID(N'[dbo].[FK_fkrol]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[usuarios] DROP CONSTRAINT [FK_fkrol];
GO
IF OBJECT_ID(N'[dbo].[FK_fkrol1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[rolpaginas] DROP CONSTRAINT [FK_fkrol1];
GO
IF OBJECT_ID(N'[dbo].[FK_fkpersonal1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[usuarios] DROP CONSTRAINT [FK_fkpersonal1];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[historials]', 'U') IS NOT NULL
    DROP TABLE [dbo].[historials];
GO
IF OBJECT_ID(N'[dbo].[mascotas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[mascotas];
GO
IF OBJECT_ID(N'[dbo].[mascotapropietarios]', 'U') IS NOT NULL
    DROP TABLE [dbo].[mascotapropietarios];
GO
IF OBJECT_ID(N'[dbo].[paginas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[paginas];
GO
IF OBJECT_ID(N'[dbo].[personals]', 'U') IS NOT NULL
    DROP TABLE [dbo].[personals];
GO
IF OBJECT_ID(N'[dbo].[propietarios]', 'U') IS NOT NULL
    DROP TABLE [dbo].[propietarios];
GO
IF OBJECT_ID(N'[dbo].[rols]', 'U') IS NOT NULL
    DROP TABLE [dbo].[rols];
GO
IF OBJECT_ID(N'[dbo].[rolpaginas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[rolpaginas];
GO
IF OBJECT_ID(N'[dbo].[usuarios]', 'U') IS NOT NULL
    DROP TABLE [dbo].[usuarios];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'historials'
CREATE TABLE [dbo].[historials] (
    [id] int IDENTITY(1,1) NOT NULL,
    [fkmascota] int  NULL,
    [fkpersonal] int  NULL,
    [tratamiento] varchar(300)  NULL,
    [fecha] datetime  NULL,
    [hora] time  NULL,
    [peso] int  NULL,
    [diagnostico] varchar(300)  NULL,
    [anamesia] varchar(200)  NULL,
    [vacuna] varchar(100)  NULL,
    [fechavacuna] datetime  NULL
);
GO

-- Creating table 'mascotas'
CREATE TABLE [dbo].[mascotas] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nombre] varchar(45)  NULL,
    [especie] varchar(45)  NULL,
    [raza] varchar(45)  NULL,
    [edad] varchar(45)  NULL,
    [color] varchar(45)  NULL,
    [sexo] varchar(45)  NULL,
    [estado] int  NULL
);
GO

-- Creating table 'mascotapropietarios'
CREATE TABLE [dbo].[mascotapropietarios] (
    [fkmascota] int  NOT NULL,
    [fkpropietario] int  NOT NULL,
    [fecha] datetime  NULL
);
GO

-- Creating table 'paginas'
CREATE TABLE [dbo].[paginas] (
    [id] int  NOT NULL,
    [nombre] varchar(45)  NULL,
    [class] varchar(100)  NULL
);
GO

-- Creating table 'personals'
CREATE TABLE [dbo].[personals] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nombre] varchar(100)  NULL,
    [direccion] varchar(45)  NULL,
    [telefono] varchar(45)  NULL,
    [correo] varchar(45)  NULL,
    [numdoc] varchar(45)  NULL,
    [tipodoc] varchar(45)  NULL,
    [estado] int  NULL
);
GO

-- Creating table 'propietarios'
CREATE TABLE [dbo].[propietarios] (
    [id] int IDENTITY(1,1) NOT NULL,
    [numdoc] varchar(45)  NOT NULL,
    [tipodoc] varchar(45)  NOT NULL,
    [nombre] varchar(100)  NOT NULL,
    [direccion] varchar(45)  NULL,
    [telefono] varchar(45)  NULL,
    [correo] varchar(45)  NULL,
    [estado] int  NULL
);
GO

-- Creating table 'rols'
CREATE TABLE [dbo].[rols] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nombre] varchar(45)  NULL
);
GO

-- Creating table 'rolpaginas'
CREATE TABLE [dbo].[rolpaginas] (
    [id] int IDENTITY(1,1) NOT NULL,
    [fkrol] int  NULL,
    [fkpagina] int  NULL
);
GO

-- Creating table 'usuarios'
CREATE TABLE [dbo].[usuarios] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nick] varchar(45)  NULL,
    [pass] varchar(45)  NULL,
    [fkrol] int  NULL,
    [estado] int  NULL,
    [fkpersonal] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'historials'
ALTER TABLE [dbo].[historials]
ADD CONSTRAINT [PK_historials]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'mascotas'
ALTER TABLE [dbo].[mascotas]
ADD CONSTRAINT [PK_mascotas]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [fkmascota], [fkpropietario] in table 'mascotapropietarios'
ALTER TABLE [dbo].[mascotapropietarios]
ADD CONSTRAINT [PK_mascotapropietarios]
    PRIMARY KEY CLUSTERED ([fkmascota], [fkpropietario] ASC);
GO

-- Creating primary key on [id] in table 'paginas'
ALTER TABLE [dbo].[paginas]
ADD CONSTRAINT [PK_paginas]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'personals'
ALTER TABLE [dbo].[personals]
ADD CONSTRAINT [PK_personals]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'propietarios'
ALTER TABLE [dbo].[propietarios]
ADD CONSTRAINT [PK_propietarios]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'rols'
ALTER TABLE [dbo].[rols]
ADD CONSTRAINT [PK_rols]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'rolpaginas'
ALTER TABLE [dbo].[rolpaginas]
ADD CONSTRAINT [PK_rolpaginas]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'usuarios'
ALTER TABLE [dbo].[usuarios]
ADD CONSTRAINT [PK_usuarios]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [fkmascota] in table 'historials'
ALTER TABLE [dbo].[historials]
ADD CONSTRAINT [FK_fkmascona]
    FOREIGN KEY ([fkmascota])
    REFERENCES [dbo].[mascotas]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_fkmascona'
CREATE INDEX [IX_FK_fkmascona]
ON [dbo].[historials]
    ([fkmascota]);
GO

-- Creating foreign key on [fkpersonal] in table 'historials'
ALTER TABLE [dbo].[historials]
ADD CONSTRAINT [FK_fkpersonal]
    FOREIGN KEY ([fkpersonal])
    REFERENCES [dbo].[personals]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_fkpersonal'
CREATE INDEX [IX_FK_fkpersonal]
ON [dbo].[historials]
    ([fkpersonal]);
GO

-- Creating foreign key on [fkmascota] in table 'mascotapropietarios'
ALTER TABLE [dbo].[mascotapropietarios]
ADD CONSTRAINT [FK_fkmascota]
    FOREIGN KEY ([fkmascota])
    REFERENCES [dbo].[mascotas]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [fkpropietario] in table 'mascotapropietarios'
ALTER TABLE [dbo].[mascotapropietarios]
ADD CONSTRAINT [FK_fkpropietario]
    FOREIGN KEY ([fkpropietario])
    REFERENCES [dbo].[propietarios]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_fkpropietario'
CREATE INDEX [IX_FK_fkpropietario]
ON [dbo].[mascotapropietarios]
    ([fkpropietario]);
GO

-- Creating foreign key on [fkpagina] in table 'rolpaginas'
ALTER TABLE [dbo].[rolpaginas]
ADD CONSTRAINT [FK_fkpagina1]
    FOREIGN KEY ([fkpagina])
    REFERENCES [dbo].[paginas]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_fkpagina1'
CREATE INDEX [IX_FK_fkpagina1]
ON [dbo].[rolpaginas]
    ([fkpagina]);
GO

-- Creating foreign key on [fkrol] in table 'usuarios'
ALTER TABLE [dbo].[usuarios]
ADD CONSTRAINT [FK_fkrol]
    FOREIGN KEY ([fkrol])
    REFERENCES [dbo].[rols]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_fkrol'
CREATE INDEX [IX_FK_fkrol]
ON [dbo].[usuarios]
    ([fkrol]);
GO

-- Creating foreign key on [fkrol] in table 'rolpaginas'
ALTER TABLE [dbo].[rolpaginas]
ADD CONSTRAINT [FK_fkrol1]
    FOREIGN KEY ([fkrol])
    REFERENCES [dbo].[rols]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_fkrol1'
CREATE INDEX [IX_FK_fkrol1]
ON [dbo].[rolpaginas]
    ([fkrol]);
GO

-- Creating foreign key on [fkpersonal] in table 'usuarios'
ALTER TABLE [dbo].[usuarios]
ADD CONSTRAINT [FK_fkpersonal1]
    FOREIGN KEY ([fkpersonal])
    REFERENCES [dbo].[personals]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_fkpersonal1'
CREATE INDEX [IX_FK_fkpersonal1]
ON [dbo].[usuarios]
    ([fkpersonal]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------