//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServicioVeterinariaRest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class personal
    {
        public personal()
        {
            this.historials = new HashSet<historial>();
            this.usuarios = new HashSet<usuario>();
        }
    
        public int id { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string correo { get; set; }
        public string numdoc { get; set; }
        public string tipodoc { get; set; }
        public Nullable<int> estado { get; set; }
    
        public virtual ICollection<historial> historials { get; set; }
        public virtual ICollection<usuario> usuarios { get; set; }
    }
}
